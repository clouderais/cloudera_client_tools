package com.cloudera.api;

import java.io.File;
import java.io.InputStream;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

import org.apache.commons.io.FileUtils;
import org.apache.cxf.Bus;
import org.apache.cxf.BusFactory;
import org.apache.cxf.jaxrs.ext.multipart.InputStreamDataSource;
import org.apache.cxf.transport.ConduitInitiatorManager;
import org.apache.cxf.transport.http.HTTPTransportFactory;

import com.cloudera.api.model.ApiCluster;
import com.cloudera.api.model.ApiConfig;
import com.cloudera.api.model.ApiConfigList;
import com.cloudera.api.model.ApiRole;
import com.cloudera.api.model.ApiService;
import com.cloudera.api.v11.RolesResourceV11;
import com.cloudera.api.v1.ServicesResource;
import com.cloudera.api.v12.ClustersResourceV12;
import com.cloudera.api.v12.RootResourceV12;
import com.cloudera.api.v4.ProcessResourceV4;

public class cm_test {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		InputStreamDataSource stream_in;
		String str_ip = null;
		String str_name = null;
		String str_pass = null;
		String file_path = "/tmp";
		boolean is_printlist = false;
		File output_file;
		if (args!=null){
			str_ip = (args.length>=1?args[0]:null);
			str_name = (args.length>=2?args[1]:null);
			str_pass = (args.length>=3?args[2]:null);
			file_path = (args.length>=4?args[3]:null);
			is_printlist = (args.length>=5?(args[4].equals("y")?true:false):false);
		}
		
		
		try{
			if (str_ip==null || str_name==null || str_pass == null){
				Scanner reader = new Scanner(System.in);  // Reading from System.in
				System.out.println("please input cloudera ip: ");
				str_ip = reader.nextLine();
				System.out.println("please input cloudera login_name:");
				str_name = reader.nextLine();
				System.out.println("please input cloudera login_pass:");
				str_pass = reader.nextLine();
				System.out.println("Are you run test: y/n");
				is_printlist = reader.nextLine().equals("y")?true:false;
				
				if (!is_printlist){
					System.out.println("export to local path: " );
					file_path = reader.nextLine();
				}
			}
			System.out.println("Run client for: " + str_ip + " and output log path: " + file_path);
			System.out.println("watting for.....................................");
			ApiRootResource root = new ClouderaManagerClientBuilder()
					.withHost(str_ip)
				    .withUsernamePassword(str_name, str_pass)
				    .build();

			System.out.println("Cloudera RootResource version: "+root.getCurrentVersion());
			
			RootResourceV12 v12 = root.getRootV12();
			ClustersResourceV12 clustersResource = v12.getClustersResource();
			int rc_count = 1;
			for (ApiCluster cluster : clustersResource.readClusters(DataView.FULL)) {
			  System.out.println(cluster.getName());
			  ServicesResource servicesResource = clustersResource.getServicesResource(cluster.getName());
			  System.out.println("There has service count: " + servicesResource.readServices(DataView.FULL).size());
			  for (ApiService service : servicesResource.readServices(DataView.FULL)) {
			    System.out.println("\t"+rc_count +": "+ service.getName());
			    if (!service.getName().equals("oozie"))continue;
			    if (servicesResource.readServiceConfig(service.getName(),DataView.FULL).size() == 0) continue;
			    
			    /**
			     * 將各service 的 properties取出
			     */
//			    List<ApiConfig> p_config_list = servicesResource.readServiceConfig(service.getName(),DataView.FULL).getConfigs();
//			    for (ApiConfig p_config: p_config_list){
//			    	String d_value = p_config.getDefaultValue();
//			    	if (d_value == null){
//			    		System.out.println("\t" + p_config.getDisplayName());
//			    		continue;
//			    	}
//			    	if (d_value.equals("false")|| d_value.equals("true")){
//			    		System.out.println("\t" + p_config.getDisplayName() + ":" +p_config.getRequired());
//			    	}else{
//			    		System.out.println("\t" + p_config.getDisplayName() + ":" +p_config.getDefaultValue());
//			    	}
//			    }
			    
			    /**
			     * 將各 role instance 的full log 取出並存檔
			     */
			    RolesResourceV11 rolesResource = (RolesResourceV11) servicesResource.getRolesResource(service.getName());
			    
			    System.out.println("\trole in "+service.getName() +" size: "+rolesResource.readRoles().size());
			    for (ApiRole role : rolesResource.readRoles()) {
//			    	System.out.println("\t\t" + role.getName()+"\t"+ v12.getHostsResource().readHost(role.getHostRef().getHostId()).getHostname());
//			    	System.out.println(service.getName()+ "-"+role.getType() +"_"+v12.getHostsResource().readHost(role.getHostRef().getHostId()).getHostname() +"_log.out");
			    	try{
			    		System.out.println("\t\tCopy remote role [" + service.getName()+ "-"+role.getType() +"_"+v12.getHostsResource().readHost(role.getHostRef().getHostId()).getHostname() +"_log.out" + "]");
				    	
			    		if (!is_printlist){
			    			InputStream inStream = rolesResource.getFullLog(role.getName());
			    			
			    			output_file = new File(file_path + File.separator + service.getName() + File.separator +service.getName()+ "-"+role.getType() +"_"+v12.getHostsResource().readHost(role.getHostRef().getHostId()).getHostname() +"_log.out");
					    	if(inStream.available()>0){
					    		System.out.println("\t\t\toutput file: "+output_file.getAbsolutePath());
					    		FileUtils.copyInputStreamToFile(inStream, output_file);
					    	}
//					    	Thread.sleep(1000);
//					    	inStream = rolesResource.getFullLog(role.getName());
//					    	if(inStream.available()>0){
//					    		System.out.println("\t\t\toutput file: "+output_file.getAbsolutePath());
//					    		FileUtils.copyInputStreamToFile(inStream, output_file);
//					    	}
//					    	
//					    	if (inStream!=null&& inStream.available()!=0){
//					    		FileUtils.copyInputStreamToFile(inStream, new File("/home/louie/temp/" + service.getName() + "/" +role.getName() +"_log.out"));
//					    	}
//					    	ProcessResourceV4 pv4 = rolesResource.getProcessesResource(role.getName());
//				    		List<String> conflist = pv4.getProcess().getConfigFiles();
//					    	for (String str: conflist){
//					    		System.out.println("\t\t\tfilename: " + str);
//					    		stream_in = pv4.getConfigFile(str);
//					    		FileUtils.copyInputStreamToFile(stream_in.getInputStream(), new File("/home/louie/temp/" + service.getName() + "/" +str));
//					    		break;
//					    	}
			    		}
			    	}catch(Exception x){x.printStackTrace();}
			    }
			    rc_count++;
			  }
			}
		}catch(Exception x){
			x.printStackTrace();
		}
	}

}
