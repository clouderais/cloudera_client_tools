package island;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

public class Cm_JsonProperty_util {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//read json file data to String
		try{
			String jsonfile = (args.length>=1?args[0]:null);
			if (jsonfile == null){
				Scanner reader = new Scanner(System.in);  // Reading from System.in
				System.out.println("please input Cloudera api service config jsonfile(file location):");
				jsonfile = reader.nextLine();
			}
			byte[] jsonData = Files.readAllBytes(Paths.get(jsonfile));

			//create ObjectMapper instance
			ObjectMapper objectMapper = new ObjectMapper();

			//read JSON like DOM Parser
			JsonNode rootNode = objectMapper.readTree(jsonData);
			Iterator<JsonNode> ii_jsnode = rootNode.elements();
			while(ii_jsnode.hasNext()){
				JsonNode p = ii_jsnode.next();
				Iterator<JsonNode> d_elments = p.elements();
				while(d_elments.hasNext()){
					JsonNode e_data = d_elments.next();
					System.out.println("<property name>"+e_data.get("name"));
					System.out.println("<value> "+ (e_data.get("value")==null?e_data.get("default"):""));
				}
			}
			
			JsonNode idNode = rootNode.path("id");
			System.out.println("id = "+idNode.asInt());

			JsonNode phoneNosNode = rootNode.path("phoneNumbers");
			Iterator<JsonNode> elements = phoneNosNode.elements();
			while(elements.hasNext()){
				JsonNode phone = elements.next();
				System.out.println("Phone No = "+phone.asLong());
			}
		}catch(Exception x){x.printStackTrace();}
		
	}

}
