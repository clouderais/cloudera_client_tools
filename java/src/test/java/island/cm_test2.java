package island;
import java.io.InputStream;
import java.util.Iterator;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.apache.cxf.jaxrs.ext.multipart.InputStreamDataSource;
 
import com.cloudera.api.ApiRootResource;
import com.cloudera.api.ClouderaManagerClientBuilder;
import com.cloudera.api.DataView;
import com.cloudera.api.model.ApiCluster;
import com.cloudera.api.model.ApiConfig;
import com.cloudera.api.model.ApiConfigList;
import com.cloudera.api.model.ApiRole;
import com.cloudera.api.model.ApiService;
import com.cloudera.api.v11.RolesResourceV11;
import com.cloudera.api.v1.ServicesResource;
import com.cloudera.api.v12.ClustersResourceV12;
import com.cloudera.api.v12.RootResourceV12;
import com.cloudera.api.v4.ProcessResourceV4;

public class cm_test2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		InputStreamDataSource stream_in;
		
		ApiRootResource root = new ClouderaManagerClientBuilder()
			    .withHost("10.1.2.2")
			    .withUsernamePassword("admin", "admin")
			    .build();
		
		RootResourceV12 v12 = root.getRootV12();
		
		ClustersResourceV12 clustersResource = v12.getClustersResource();
		for (ApiCluster cluster : clustersResource.readClusters(DataView.FULL)) {
		  System.out.println(cluster.getName());
		  ServicesResource servicesResource = clustersResource.getServicesResource(cluster.getName());
		 
		  
		  
		
		  
		  
		  for (ApiService service : servicesResource.readServices(DataView.FULL)) {
		    System.out.println("\t" + service.getName());
		    try{
				  InputStreamDataSource clientConfig =  clustersResource.getClientConfig(service.getName());
				  ZipEntry zipEntry = null; 
				  ZipInputStream inputStream = new ZipInputStream(clientConfig.getInputStream());
				  while ((zipEntry = inputStream.getNextEntry()) != null) {
					  System.out.println("Reading: " + zipEntry.getName()); 
				  }
			  }catch(Exception x){
				  x.printStackTrace();
			  }
		    
		    
		    
//		    RolesResourceV11 rolesResource = (RolesResourceV11) servicesResource.getRolesResource(service.getName());
//		    
//		    for (ApiRole role : rolesResource.readRoles()) {
//		    	System.out.println("\t\t" + role.getName());
//		    	
//		    	ProcessResourceV4 pv4 = rolesResource.getProcessesResource(role.getName());
//		    	
//		    	try{
//		    		List<String> conflist = pv4.getProcess().getConfigFiles();
//			    	for (String str: conflist){
//			    		System.out.println("\t\t\tfilename: " + str);
//			    		stream_in = pv4.getConfigFile(str);
//			    	}
//		    	}catch(Exception x){x.printStackTrace();}
//		    }
		  }
		}

	}

}
